#include<iostream>
#include<math.h>
// toa do oxy
using namespace std;
int main(){
	int n;
	cout << "Nhap so diem: ";
	cin >> n;
	float x[n], y[n];
	// nhap list toa do diem
	for(int i=0; i<n; i++){
		cout << "Nhap diem thu "<< i+1 << ": ";
		cin >> x[i] >> y[i];
	}
	cout << "\t" << "So diem vua nhap: ";
	// a. in lits toa do diem
	for(int i=0; i<n; i++){
		cout << "(" << x[i] << ", " << y[i] << "), ";
	}
	// b. dem so diem duoi Ox
	int dem=0;
	cout << "\ndiem duoi Ox: ";
	for(int i=0; i<n; i++){
		if(y[i]<0){
			dem = dem + 1;
			cout << "(" << x[i] << ", " << y[i] << "), ";
		}
	}
	cout << "\nSo diem duoi Ox: " << dem << endl;
	// c. khoang cach xa nhat tu diem O
	float kc;
	int i;
	float max;
	max = sqrt(pow(x[0],2) + pow(y[0],2));
	for(int i=0; i<n; i++){
		kc=sqrt(pow(x[i],2) + pow(y[i],2));
		if(max < kc){
			max = kc;
		}
	}
	cout << "Khoang cach lon nhat tu goc O(0,0): " << max << endl;
}
