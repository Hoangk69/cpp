#include<iostream>
#include<string>
using namespace std;

int main(){
	string s;
	cout << "Nhap xau s: ";
	getline(cin,s);
	for(int i=0; i < s.length(); i++){
		cout << s[i] << "\n"; // in ra tung ki tu cua xau s
	}
	cout << "Do doi xau: " << s.length() << endl;
	s.erase(s.begin() + 3); // xoa 1 ki tu trong xau tai vi tri so 3
	cout << "Xau sau khi xoa: " << s << endl;
	cout << "Do dai xau sau khi xoa: " << s.length() << endl;
	s.insert(s.begin() + 3, 'H'); // chen 1 ki tu H vao vi tri so 3
	cout << "Xau sau khi chen 1 ki tu H: " << s << endl;
	cout << "Do dai xau sau khi chen H: " << s.length();
	
}
