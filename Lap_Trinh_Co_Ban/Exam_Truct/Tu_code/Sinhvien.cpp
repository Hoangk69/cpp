#include<iostream>
#include<string>
#define MAX 200
using namespace std;

struct Sinhvien{
	string hoten;
	float diemtoan, diemly, diemhoa;
	
};
void Nhap(Sinhvien &a){
	cout << "Nhap ten sinh vien: ";
	getline(cin, a.hoten);
	cout << "Nhap diem toan: ";
	cin >> a.diemtoan;
	cout << "Nhap diem ly: ";
	cin >> a.diemly;
	cout << "Nhap diem hoa: ";
	cin >> a.diemhoa;
}
void Xuat(Sinhvien a){
	cout << "\n=============\n";
	cout << "Ten sv: " << a.hoten << endl;
	cout << "Toan: " << a.diemtoan << endl;
	cout << "Ly: " << a.diemly << endl;
	cout << "Hoa: " << a.diemhoa << endl;
}
float S(Sinhvien a){
	return (a.diemtoan+a.diemly+a.diemhoa);
}
void SX(Sinhvien a[], int n){
	Sinhvien x;
	for(int i=0; i<n-1; i++){
		for(int j=i+1; j<n; j++){
			if(S(a[i]) < S(a[j])){
				x=a[i];
				a[i]=a[j];
				a[j]=x;
			}
		}
	}
}
string GetName(string s){
	for(int i=s.length()-1; i>=0; i--){
		if(s[i]==' '){
			return s.substr(i+1);
		}
	}
	return s;
}

int main(){
	int n;
	cout << "Nhap so sinh vien: ";
	cin >> n;
	cin.ignore();
	Sinhvien sv[MAX];
	for(int i=0; i<n; i++){
		Nhap(sv[i]);
		cin.ignore();
	}
	for(int i=0; i<n; i++){
		Xuat(sv[i]);
		cout << endl;
		cout <<"Tong: " << S(sv[i]) << endl;
	}
	cout << "Danh sach SV theo thu tu tong diem giam dan: ";	
	SX(sv, n);
	for(int i=0; i<n; i++){
		Xuat(sv[i]);
	}
	
	cout << "Nhung ban co ten la An la: ";
	for(int i=0; i<n; i++){
		if(GetName(sv[i].hoten)=="An"){
			Xuat(sv[i]);
		}
	}
//	Sinhvien sv;
//	Nhap(sv);
//	Xuat(sv);
}
