#include <iostream>
using namespace std;

int main(){
	int n,m;
	
	do{
		cout << "Nhap so dong n: ";
		cin >> n;
	}
	while (n<1);
	
	do{
		cout << "Nhap so cot m: ";
		cin >> m;
	}
	while (m>100);

	int a[n][m];
	
	// nhap du lieu
	for (int i=0; i<n; i++){
		for (int j=0; j<m; j++){
			cout << "Nhap gia tri co vat: " << "[" << i << "]" << "[" << j << "]"<< endl;
			cin >> a[i][j];
		}
	}
	// in ra 
	for (int i=0; i<n; i++){
		for (int j=0; j<m; j++){
			cout << a[i][j] << "\t";
		}
		cout << endl;
	}
	
	int x=0, max=a[0][0];      // x la gia tri co vat bang 0
	
	for (int i=0; i<n; i++){
		for (int j=0; j<m; j++){
			if(a[i][j] ==0){
				x++;
			}
			if(max < a[i][j]){
				max=a[i][j];
			}
		}
	}
	cout << "\nSo vat khong gia tri: " << x << endl;
	cout << "Vat co gia tri lon nhat: " << max << endl;
}
