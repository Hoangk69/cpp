#include <iostream>
using namespace std;

int main (){
	int n, i;	
	do{
		cout << "Nhap so luong thung chua n trong khoang 1 den 1000: ";
		cin >> n;
	} 
	while (n<1 || n>1000);
	
	float a[n] , b[n] ;
	for (i=0; i<n; i++){
		do{
			cout << "Nhap suc chua cua thung thu " << i+1 << endl;
			cin >> a[i];
		}
		while (a[i]<1 || a[i]>1000);
		do{
			cout << "Nhap so lit con lai cua thung thu " << i+1 << endl;
			cin >> b[i];
		}
		while (b[i] < 0 || b[i] > a[i]);
	}
	// cau b x: day ; y: het ; z: voi ;
	int x=0, y=0 , z=0; 

	float S=0;  // so lit can bo sung de day. 
	
	for (i=0; i<n; i++){
		if (b[i]==0){
			y=y+1;
		}
		else if (b[i]<a[i]){
			z=z+1;
		}
		else {
			x=x+1;    // b[i] == a[i]
		}	
		
		S+=a[i]-b[i];  // cau c
	}
	cout << "so luong con day: " << x << endl;
	cout << "so luong voi: " << z << endl;
	cout << "so luong het: " << y << endl;
	cout << "Tong so luong lit bo sung cac thung la: " << S << endl;
}
