#include <iostream>
#include <math.h>
using namespace std;

int main(){
	float x;
	int n;
	cout << "Nhap x: ";
	cin >> x;
	cout << "Nhap n: ";
	cin >> n;
	float S=1;
	
	for (int i=1; i<=n; i++){
		S+= pow(-1,i) * pow(x, 2*i) / (2*i);
	}
	cout << "S(x,n): " << S;
}
