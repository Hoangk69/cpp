#include<iostream>
#include<fstream>
using namespace std;

bool CheckSNT(int x){
	int dem_uoc=0;
	for(int i=1; i<=x; i++){
		if(x%i==0){
			dem_uoc++;
		}
	}
	if(dem_uoc==2){
		return true;
	}
	else{
		return false;
	}
}

int main(){
	ifstream file_in;  // doc du lieu tu file
	file_in.open("INPUT.txt", ios::in);
	if(file_in.fail()){
		cout << "Khong mo file thanh cong! ";
		return 0;
	}
	int n;
	file_in >> n; // doc so nguyen tu file 
	file_in.close(); // dong file
	
	// ghi du lieu
	ofstream file_out; // ghi file
	file_out.open("KETQUA.txt", ios::out);
	for(int i =1; i<=n; i++){
		if(CheckSNT(i)){
			file_out << i << "\t";
		}
	}
	file_out.close(); // dong file
	
}
