#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
using namespace std;
bool sohoanhao(int n){
	int sum_uoc =0;
	for(int i=1; i<n; i++){
		if(n%i==0){
			sum_uoc+=i;
		}
	}
	if(sum_uoc==n){
		return true;
	}
	else{
		return false;
	}
}
main(){
	ifstream f;
	f.open("NUMBER.INP");  // mo file
	if (f.fail()) {
		cout << "Khong mo file thanh cong !";
		return 0;
	}
	string s;
	cout << "Du lieu doc duoc tu file la: " << endl;
	
	while (!f.eof()) {     // doc file
		getline(f, s); 
		cout << s << endl; 
	}
	f.close();  // dong file
	
	fstream f_out;
	f_out.open("NUMBER.OUT", ios::out);  // mo file
	if (f_out.fail()) {
		cout << "Khong mo file thanh cong !";
		return 0;
	}
	
	int m=atoi(s.c_str());   // ep kieu: string to int 
	for(int i=1; i<m; i++){      
		if(sohoanhao(i)){
			f_out << i << "\n";  // ghi file
		}
	}
	f_out.close();
}
