#include <iostream>
using namespace std;
int phepcong(int x, int y) {
	return x + y;
}
int pheptru(int x, int y) {
	return x - y;
}
// khai bao ham co su dung tham so la con tro ham
int pheptoan(int x, int y, int (*phep_toan_can_goi)(int, int)) {
	int z;
	z = (*phep_toan_can_goi)(x, y);
	return z;
}
// ham main
int main ()
{
int m,n;
// khai bao mot con tro ham, tro den ham pheptru
int (*pheptru_Ptr)(int, int) = pheptru;
// goi ham pheptoan voi tham so thu 3 la con tro cua 1 ham khac
m = pheptoan(15, 20, &phepcong);
n = pheptoan(40, 18, pheptru_Ptr);
cout << "m = " << m << endl;
cout << "n = " << n << endl;
return 0;
}

