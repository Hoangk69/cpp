#include <iostream>
using namespace std;
int cong2so(int *a, int b) {
	*a = *a + 2; // tang a them 2
	b = b + 4; // tang b them 4
	return *a + b;
}

main() {
	int a, b, c;
	a = 15; b = 20;
	c = cong2so(&a, b); // goi ham cong2so
	cout << "a =  " << a << endl;
	cout << "b = " << b << endl;
	cout << "Tong cua a+b la: " << c;
	return 0;
}
