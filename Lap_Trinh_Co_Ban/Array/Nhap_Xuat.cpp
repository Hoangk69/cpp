#include<iostream>
using namespace std;

void NhapMang(int a[], int n){
	for(int i=0; i<n; i++){
		cout << "Nhap phan tu thu " << i+1 << ": ";
		cin >> a[i];
	}
}
void XuatMang(int a[], int n){
	for(int i=0; i<n; i++){
		cout << "Phan tu thu " << i+1 << ": ";
		cout << a[i] << endl;
	}
}
int main(){
	int n;
	cout << "Nhap so phan tu: ";
	cin >> n;
	int a[n];
	NhapMang(a,n);
	cout << endl;
	XuatMang(a,n);
	cout << endl;
	system("Pause");
	return 0;
}
