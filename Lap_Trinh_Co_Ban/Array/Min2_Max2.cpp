#include<iostream>
using namespace std;

void NhapMang(int a[], int n){
	for(int i=0; i<n; i++){
		cout << "Nhap phan tu thu " << i+1 << ": ";
		cin >> a[i];
	}
}
void XuatMang(int a[], int n){
	cout << "\nXuat mang: ";
	for(int i=0; i<n; i++){
		cout << a[i] << "\t";
	}
}
void SapXep(int a[], int n){  // sap xep
	int t=0;
	for(int i = 0; i<n-1; i++){
		for (int j=i+1; j<n; j++){
			if(a[i]>a[j]){
				t= a[i];
				a[i]=a[j];
				a[j]=t;
			}
		}
	}
}
void InSapXep(int a[], int n){  // sap xep tang dan
	SapXep(a,n);
	XuatMang(a,n);
}

int max1(int a[], int n){
	return a[n-1];    // mang duoc sap xep tu be den lon
	
}
int min1(int a[], int n){
	return a[0];		// mang duoc sap xep tu be den lon
}

int min2(int a[], int n){
	for(int i=1; i<n; i++){
		if(a[i]!=a[0]){
			return a[i];
		}
	}
	return 0;
}

int max2(int a[], int n){
	for(int i=n-2; i >= 0; i--){
		if(a[i]!=a[n-1]){
			return a[i];          // 1 1 2 3 3 4 5 5
		}
	}
	return 0;
}

int main(){
	int n;
	cout << "Nhap so phan tu: ";
	cin >> n;
	int a[n];
	NhapMang(a,n);
	cout << endl;
	XuatMang(a,n);
	cout << "Mang sau khi sap xep la: ";
	InSapXep(a,n);
	cout << "Max1: "<<max1(a,n) << endl;
	cout << "Min1: "<<min1(a,n) << endl;
	cout << "\nMax2: " << max2(a,n) << endl;
	cout << "Min2: " << min2(a,n) << endl;
	cout << endl;
	system("Pause");
	return 0;
}
