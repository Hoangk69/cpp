#include<iostream>
#define MaxDong 100
#define MaxCot 100
using namespace std;

// nhap mang
void NhapMang(int a[][MaxCot],int n, int m){
	for(int i=0; i<n; i++){
		for(int j=0; j<m; j++){
			cout << "Nhap mang: a[" << i << "]" << "[" << j << "]: ";
			cin >> a[i][j];
		}
	}
	cout << endl;
}
// xuat mang
void XuatMang(int a[][MaxCot], int n, int m){
	for(int i=0; i<n; i++){
		for(int j=0; j<m; j++){
			cout << a[i][j] << "\t";
		}
		cout << endl;
	}
	cout << endl;
}
// check 1 so co phai la so Nguyen To ko
bool CheckSNT(int x){
	int dem_uoc=0;
	for(int i=1; i<=x; i++){
		if(x%i==0){
			dem_uoc++;  
		}
	}
	if(dem_uoc==2){
		return true;
	}
	else{
		return false;
	}
}
// tinh tong cac so nguyen to trong mang 2 chieu
int TongSNT(int a[][MaxCot], int n, int m){
	int T=0;
	for(int i=0; i<n; i++){
		for(int j=0; j<m; j++){
			if(CheckSNT(a[i][j])){
				T+=a[i][j];			
			}
		}
	}
	return T;
}
void TongTungDong(int a[][MaxCot], int n, int m){
	for(int i=0; i<n; i++){
		int Td=0;
		for(int j=0; j<m; j++){
			Td+=a[i][j];              // dao i vs i -> Td+=a[j][i] tong tung cot
		}
		cout << "Tong dong thu " << i+1 << " : " << Td << endl;
	}
}
void InDuongCheoChinh(int a[][MaxCot], int n, int m){ // ma tran vuong n=m
	if(n!=m){
		cout << "Khong co duong cheo chinh" << endl;
		return;
	}
	cout << "Duong cheo chinh la: " << endl;
	for(int i=0; i<n; i++){
		cout << a[i][i] << "\t";
	}
}
int main(){
	int n, m;
	cout << "Nhap n: ";
	cin >> n;
	cout << "Nhap m: ";
	cin >> m;
	int a[MaxDong][MaxCot];
	NhapMang(a,n,m);
	XuatMang(a,n,m);
	cout << TongSNT(a,n,m) << endl;
	TongTungDong(a,n,m);
	InDuongCheoChinh(a,n,m);
	system("Pause");
	cout << "\n";
}
