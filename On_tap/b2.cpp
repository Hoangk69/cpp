#include<iostream>
#include<string>
using namespace std;

class SINHVIEN{
	private:
		string hoten;
		int namsinh;
		double DTB;
		bool gioitinh;
	public:
		SINHVIEN(){
			hoten="";
			namsinh=0;
			DTB=0;
			gioitinh=0;
		}
		SINHVIEN(string hoten, int namsinh, double DTB, bool gioitinh){
			this->hoten=hoten;
			this->namsinh=namsinh;
			this->DTB=DTB;
			this->gioitinh=gioitinh;
		}
		void Nhap();
		void Hienthi();
		string gethoten(){
			return this->hoten;
		}
		int getnamsinh(){
			return this->namsinh;
		}
		friend void Sosanh(SINHVIEN A, SINHVIEN B);
};
void SINHVIEN::Nhap(){
	cout << "Nhap ho ten: ";
	getline(cin, this->hoten);
	cout << "Nhap nam sinh: ";
	cin>>this->namsinh;
	cout << "Nhap DTB: ";
	cin>>this->DTB;
	cout << "Nhap gioi tinh: ";
	cin>>this->gioitinh;
}
void SINHVIEN::Hienthi(){
	cout << hoten << " - "<< namsinh << " - "<< DTB << " - "<< gioitinh << endl;
}
void Sosanh(SINHVIEN A, SINHVIEN B){
	if(A.namsinh < B.namsinh){
		cout <<"A > B";
	}
}
int main(){
	int n;
	cout << "Nhap so luong sinh vien: ";
	cin>>n;
	cin.ignore();
	SINHVIEN A[n];
	for(int i=0; i<n; i++){
		A[i].Nhap();
		cin.ignore();
	}
	int dem=0;
	for(int i=0; i<n; i++){
		if(A[i].gethoten()=="Binh" && A[i].getnamsinh()==1982){
			dem++;
		}
	}
	cout << "So luong sv ten Binh va sinh nam 1982: " << dem << endl;
	SINHVIEN t;
	for(int i=0; i<n-1; i++){
		for(int j=i+1; j<n; j++){
			if(A[i].getnamsinh()<A[j].getnamsinh()){
				t=A[i];
				A[i]=A[j];
				A[j]=t;
			}
		}
	}
	for(int i=0; i<n; i++){
		A[i].Hienthi();
	}
}

