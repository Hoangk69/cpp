#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Sach{
	private:
		string ten, nxb;
		int namXb, soLuong;
		bool ttMuon;
	public:
		//Ham tao
		Sach();
		Sach(string, string, int, int, bool);
		//Ham Getter, Setter
		string getTen();
		void setTen(string _ten);
		string getNxb();
		void setNxb(string _nxb);
		int getNamXb();
		void setNamXb(int _namXb);
		int getSoLuong();
		void setSoLuong(int _soLuong);
		bool getTTMuon();
		void setTTMuon(bool _ttMuon);
		//Ham nhap, xuat
		void nhap();
		void xuat();
		void copy(Sach);
};
Sach::Sach(){
	this->ten = this->nxb = "null";
	this->namXb = this->soLuong = 0;
	this->ttMuon = false;
}
Sach::Sach(string _ten, string _nxb, int _namXb, int _soLuong, bool _ttMuon){
	this->ten = _ten;
	this->nxb = _nxb;
	this->namXb = _namXb;
	this->soLuong = _soLuong;
	this->ttMuon = _ttMuon;
}
string Sach::getTen(){
	return this->ten;
}
void Sach::setTen(string _ten){
	this->ten = _ten;
}
string Sach::getNxb(){
	return this->nxb;
}
void Sach::setNxb(string _nxb){
	this->nxb = _nxb;
}
int Sach::getNamXb(){
	return this->namXb;
}
void Sach::setNamXb(int _namXb){
	this->namXb = _namXb;
}
int Sach::getSoLuong(){
	return this->soLuong;
}
void Sach::setSoLuong(int _soLuong){
	this->soLuong = _soLuong;
}
bool Sach::getTTMuon(){
	return this->ttMuon;
}
void Sach::setTTMuon(bool _ttMuon){
	this->ttMuon = _ttMuon;
}
void Sach::nhap(){
	cin.ignore();
	cout << "----------Nhap sach----------\n";
	cout << "Nhap ten sach: ";
	getline(cin, this->ten);
	cout << "Nhap nha xuat ban: ";
	getline(cin, this->nxb);
	cout << "Nhap nam xuat ban: ";
	cin >> this->namXb;
	cout << "Nhap so luong sach: ";
	cin >> this->soLuong;
	cout << "Nhap trang thai muon sach: ";
	cin >> this->ttMuon;
}
void Sach::xuat(){
	cout << "Sach [ " << this->ten << "|" << this->nxb << "|" 
	<< this->namXb << "|" << this->soLuong << "|" << this->ttMuon << " ]\n";
}
void ghiFile(Sach listSach[], int n){
	ofstream ghiFile;
	ghiFile.open("list.txt", ios::out);
	ghiFile << n << endl;
	for(int i = 0; i < n; i++){
		ghiFile << listSach[i].getTen() << " - " 
		<< listSach[i].getNxb() << " - " 
		<< listSach[i].getNamXb() << " - " 
		<< listSach[i].getSoLuong() << " - " 
		<< listSach[i].getTTMuon() << endl;
	}
	ghiFile.close();
}
void Sach::copy(Sach s){
	this->ten = s.ten;
	this->nxb = s.nxb;
	this->namXb = s.namXb;
	this->soLuong = s.soLuong;
	this->ttMuon = s.ttMuon;
}
int main(){
	Sach listSach[100];
	int n;
	cout << "So luong sach: ";
	cin >> n;
	for(int i = 0; i < n; i++){
		listSach[i].nhap();
	}
	for(int i = 0; i < n; i++){
		listSach[i].xuat();
	}
	ghiFile(listSach, n);
	//Sap xep theo ten sach
	for(int i = 0; i < n-1; i++)
	{
		for(int j = i+1; j < n; j++){
			if(listSach[i].getTen() > listSach[j].getTen()){
				Sach s = Sach();
				s.copy(listSach[i]);
				listSach[i].copy(listSach[j]);
				listSach[j].copy(s);
			}
		}
	}
	for(int i = 0; i < n; i++){
		listSach[i].xuat();
	}
	return 0;
}
