#include<iostream>
using namespace std;
class LoDat{
	public:
		float ChieuDai;
		float ChieuRong;
		LoDat(float cd=1.0, float cr=1.0){
			this->ChieuDai=cd;
			this->ChieuRong=cr;
		}
		void NhapDL(){
			cout<<"Nhap vao chieu dai lo dat: ";
			cin>>ChieuDai;
			cout<<"Nhap vao chieu rong lo dat: ";
			cin>>ChieuRong;
		}
		void InDL(){
			cout<<ChieuDai<<"x"<<ChieuRong<<endl;
		}
		float DienTich(float ChieuDai,float ChieuRong ){
			  float DienTich=ChieuDai*ChieuRong;
			  return ChieuDai*ChieuRong;
		}
};
int main(){
	int n;
	cout<<"Nhap so luong lo dat: ";
	cin>>n;
	
	LoDat List[200];
	for(int i=0;i<n;i++){
		cout<<"Nhap kich thuoc lo dat "<<i+1<<": "<<endl;
		List[i].NhapDL();
		cout<<endl;
		List[i].DienTich(List[i].ChieuDai,List[i].ChieuRong);
		
		
	}
	float max=List[0].DienTich(List[0].ChieuDai,List[0].ChieuRong);
	int a=0;
	long sum=0;
	cout<<"Cac lo dat co dien tich lon hon 200m2 la: ";
	for(int i=0;i<n;i++){
		if(List[i].DienTich(List[i].ChieuDai,List[i].ChieuRong)>200){
			List[i].InDL();
		}
		if(List[i].DienTich(List[i].ChieuDai,List[i].ChieuRong)>max){
			max=List[i].DienTich(List[i].ChieuDai,List[i].ChieuRong);
		}
		sum+=List[i].DienTich(List[i].ChieuDai,List[i].ChieuRong);
		
	}
	for(int i=0;i<n;i++){
		if((List[i].DienTich(List[i].ChieuDai,List[i].ChieuRong))==max){
			cout<<"Lo dat so  "<<i+1<<" co gia tri lon nhat "<<endl;
		}
	}
	cout<<"Tong thue la: "<<sum*1000<<"dong"<<endl;


}
