#include<iostream>
using namespace std;

class TDate{
	// thuoc tinh
private: 
	short ngay, thang, nam;
	// phuong thuc
public:
	TDate(){
	}
	TDate(short ngay, short thang, short nam); // ham tao constructor
	TDate(TDate &d); // ham tao sao cheop
	void NhapDL();
	void Hien_Thi();
	friend void sosanh(TDate d1, TDate d2);
	short get_ngay(){
		return this->ngay;
	}
	short get_thang(){
		return this->thang;
	}
	short get_nam(){
		return this->nam;
	}
};
TDate::TDate(short ngay, short thang, short nam){
	this->ngay=ngay;
	this->thang=thang;
	this->nam=nam;
}
TDate::TDate(TDate &d){
	this->ngay=d.get_ngay();
	this->thang=d.get_thang();
	this->nam=d.get_nam();
}
void TDate::NhapDL(){
	cout << "Nhap ngay: ";
	cin >> this->ngay;
	cout << "Nhap thang: ";
	cin >> this->thang;
	cout << "Nhap nam: ";
	cin >> this->nam;
}
void TDate::Hien_Thi(){
	cout << "Hien thi: " << ngay << "/" << thang << "/" << nam;
}
void sosanh(TDate d1, TDate d2){
	if(d1.nam>d2.nam){
		cout << "d2 som hon d1" << endl;
		d2.Hien_Thi();
	}
	else if(d1.nam==d2.nam){
		if(d1.thang>d2.thang){
			cout << "d2 som hon d1" << endl;
			d2.Hien_Thi();
		}
		else if(d1.thang==d2.thang){
			if(d1.ngay>d2.ngay){
				cout << "d2 som hon d1" << endl;
				d2.Hien_Thi();
			}
			else if(d1.ngay==d2.ngay){
				cout << "d1 bang d2" << endl;
				d1.Hien_Thi();
			}
			else{
				cout << "d1 som hon d2" << endl;
				d1.Hien_Thi();
			}
		}
		else{
			cout << "d1 som hon d2" << endl;
			d1.Hien_Thi();
		}
	}
	else{
		cout << "d1 som hon d2" << endl;
		d1.Hien_Thi();
	}
}
