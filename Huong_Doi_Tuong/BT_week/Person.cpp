#include<iostream>
#include<string>
#include "TDate.h"
using namespace std;

class Person{	
private: 
	string HoTen;
	TDate *NgaySinh;
	string QueQuan;
public: 
	Person(){
		this->NgaySinh=new TDate;
	}
	Person(string HoTen, TDate NgaySinh, string QueQuan);
	void Nhap();
	void Xuat();
};
Person::Person(string HoTen, TDate NgaySinh, string QueQuan){
	this->HoTen=HoTen;
	this->NgaySinh=new TDate(NgaySinh);
	this->QueQuan=QueQuan;
}
void Person::Nhap(){
	cout << "Nhap Ho ten: ";
	getline(cin,HoTen);
	this->NgaySinh->NhapDL();
	cin.ignore();
	cout << "Nhap Que Quan: ";
	getline(cin, QueQuan);
}
void Person::Xuat(){
	cout << "\nHo Ten: " << this->HoTen;
	cout << "\nNgay sinh: " ; this->NgaySinh->Hien_Thi();
	cout << "\nQue Quan: " << this->QueQuan;
}
int main(){
	Person a;
	a.Nhap();
	a.Xuat();
}

