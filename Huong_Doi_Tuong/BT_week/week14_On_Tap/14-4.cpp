#include<iostream>
using namespace std;

class FRACTION{
	private:
		int tuso, mauso;
	public:
		FRACTION(int tuso, int mauso){
			this->tuso=tuso;
			this->mauso=mauso;
		}
		FRACTION(){
			this->tuso=0;
			this->mauso=1;
		}
		FRACTION(FRACTION &a){
			this->tuso=a.tuso;
			this->mauso=a.mauso;
		}
		void rutgon(){
			for(int i= this->tuso; i>1; i--){
				if(this->tuso%i==0 && this->mauso%i==0){
					this->tuso=tuso/i;
					this->mauso=mauso/i;
				}
			}
		}
		friend istream & operator >> (istream &is, FRACTION &c);		
		friend ostream & operator << (ostream &is, FRACTION &c);
		FRACTION operator+(FRACTION);
};
FRACTION FRACTION::operator+(FRACTION x){
	FRACTION tmp;
	tmp.tuso=this->tuso*x.mauso;
	tmp.mauso=this->mauso*x.tuso;
	return tmp;
}
istream & operator >> (istream &is, FRACTION &c){
	cout << "Nhap tu so: ";
	is>> c.tuso;
	cout << "Nhap mau so: ";
	is>> c.mauso;
	return is;
}		
ostream & operator << (ostream &os, FRACTION &c){
	os << c.tuso << "/" << c.mauso << endl;
	return os;
}

int main(){
	FRACTION A, B, C;
	cin >> A;
	cout << A;
	
	cin >> B;
	cout << B;
	
	cout << "A + B = ";
	C=A+B;
	cout << C;
	cout << "Rut gon: ";
	A.rutgon();
	cout << A;
}
