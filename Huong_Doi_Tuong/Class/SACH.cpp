#include <iostream>
#include <iostream>
#define MAX 100
using namespace std;

class SACH{
	private:
		string tenSach;
		string tenNXB;
		int nam;
		int soluong;
		bool TTM;
	public:
		SACH(){
			this->tenSach="";
			this->tenNXB="";
			this->nam=0;
			this->soluong=0;
			this->TTM=0;
		}
		SACH(string tenSach, string tenNXB, int nam, int soluong, bool TTM){
			this->tenSach=tenSach;
			this->tenNXB=tenNXB;
			this->nam=nam;
			this->soluong=soluong;
			this->TTM=TTM;
		}
		void Nhap();
		void Xuat();
		int getnam(){
			return this->nam;
		}
		bool getTTM(){
			return this->TTM;
		}
		void SX(SACH a[], int n){
			SACH x;
			for (int i=0; i<n-1; i++){
				for(int j=i+1; j<n; j++){
					if(a[i].tenSach < a[j].tenSach){
						x=a[i];
						a[i]=a[j];
						a[j]=x;
					}
				}
			}
		}
		string gettenSach(){
			return this->tenSach;
		}
};
void SACH::Nhap(){
	cout << "Nhap ten sach: ";
	getline(cin, this->tenSach);
	cout << "Ten nha xuat ban: ";
	getline(cin, this->tenNXB);
	cout << "Nhap nam xuat ban: ";
	cin >> this->nam;
	cout << "Nhap so luong: ";
	cin >> this->soluong;
	cin.ignore();
	cout << "Trang thai muon: ";
	cin >> this->TTM;
}
void SACH::Xuat(){
	cout << this->tenSach << " - " << this->tenNXB << " - " << this->nam << " - " << this->soluong << " - " << this->TTM << endl;
}

int main(){
	int n;
	cout << "Nhap so quyen sach: ";
	cin >> n;
	cin.ignore();
	SACH LIST[MAX];
	// nhap 
	for (int i=0; i<n; i++){
		LIST[i].Nhap();
		cin.ignore();
		cout << "\n------------------\n";
	}
	// in ra nhung sach da bi muon
//	cout << "Nhung quyen sach da bi muon " << endl;
//	
//	for (int i=0; i<n; i++){
//		if(LIST[i].getTTM()){
//			LIST[i].Xuat();
//			cout << "\n------------------\n";	
//		}	
//	}
	
	// in ra sach xuat ban truoc 1990
//	cout << "Nhung quyen sach xuat ban truoc 1990 " << endl;
//	
//	for (int i=0; i<n; i++){
//		if(LIST[i].getnam() < 1990){
//			LIST[i].Xuat();
//			cout << "\n------------------\n";	
//		}
//	}
//	SX(LIST,n);
	// sap xep theo ten sach 
	SACH x;
	for (int i=0; i<n-1; i++){
		for(int j=i+1; j<n; j++){
			if(LIST[i].gettenSach() < LIST[j].gettenSach()){
				x=LIST[i];
				LIST[i]=LIST[j];
				LIST[j]=x;
			}
		}
	}
	// xuat 
	for(int i=0; i<0; i++){
		LIST[i].Xuat();
		cout << "\n====================\n";
	}
}
