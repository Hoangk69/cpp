#include<iostream>
#include<string>
using namespace std;

class HOCSINH{
	private: 
		string hoten;
		float DiemToan, DiemLy, DiemHoa, DiemTB;
	public:
		void NhapDL(){
			cout << "Nhap ten: ";
			getline(cin,hoten);
			cout << "Nhap diem toan: ";
			cin >> DiemToan;
			cout << "Nhap diem ly: ";
			cin >> DiemLy;
			cout << "Nhap diem hoa: ";
			cin >> DiemHoa;
		}
		float TB(){
			DiemTB=(DiemToan+DiemLy+DiemHoa)/3;
			return DiemTB;
		}
		void PhanLoai(){
			if(TB() < 5){
				cout << "Hoc Sinh Yeu";
			}
			else if(TB() >=5 && TB() < 7){
				cout << "Hoc sinh TB";
			}
			else if(TB() >=7 && TB() < 8){
				cout << "Hoc sinh Kha";
			}
			else {
				cout << "Hoc Sinh gioi";
			}
			cout << endl;
		}
		void HienThiThongTin(){
			cout << "\n-------------------------\n";
			cout << "Ho ten SV: " << hoten << endl;
			cout << "Diem Toan: "<<DiemToan<<endl;
			cout << "Diem Ly: "<<DiemLy<<endl;
			cout << "Diem Hoa:" <<DiemHoa<<endl;
			cout << "Diem TB: " << TB() << endl;
			PhanLoai();
		}
};
int main(){
	HOCSINH a;
	a.NhapDL();
	a.TB();
	a.HienThiThongTin();
}

