// vi du 7.1
#include <iostream>
#include <math.h>
using namespace std;
//  dinh nghia lop so phuc
class complex {
      double a, b;   // x = a + b * i
public:
      complex() { a = 0; b = 0;} 
      complex(double, double);
      complex operator+(complex); // ham toan tu cong
      complex operator-(complex);  // ham toan tu tru    
      void operator+=(complex); // thay doi gia tri so phuc 
      complex operator+(double z); // cong mot so phuc voi so thuc
      friend complex operator-(complex,double);
      void display();      
};
// trien khai cac ham thanh phan
complex::complex(double a, double b) {
	this->a = a;
	this->b = b;
}
complex complex::operator+(complex x) {
   complex tmp;
   tmp.a = a + x.a;
   tmp.b = b + x.b;
   return tmp; 
}
complex complex::operator-(complex x) {
   complex tmp;
   tmp.a = a - x.a;
   tmp.b = b - x.b;
   return tmp;              
}
void complex::operator+=(complex x){
	a = a + x.a;
   	b = b + x.b;  
}
complex complex::operator+(double z){
	complex tmp;
   	tmp.a = a + z;
   	tmp.b = this->b;
   	return tmp; 
}
complex operator-(complex x,double z){
	complex tmp;
	tmp.a= x.a - z;
	tmp.b= x.b;
	return tmp;
}
void complex::display() {
     cout << a << " + " << b << " * i" << endl;     
}
main() {
  complex A(4, 8);
  complex B(3, 4);
  complex C;
  cout << " A = "; 
  A.display();
  cout << " B = ";
  B.display();
  
//  cout << "A+=B ";
//  A+=B;
//  A.display();
//  cout << " C = ";
//  C = A + B; //C = A.operator+(B);
//  C.display();
  cout << "A+10= ";
  C= A.operator+(10);
  C.display();
  C=A-10;
  C.display();
  return 0;
}
