#include<iostream>
using namespace std;

class PHANSO{
private:	
	int ts,ms;
public:
	PHANSO(){
		ts=0;
		ms=1;
	};
	PHANSO(int ts, int ms);
	PHANSO operator+(PHANSO);
	PHANSO operator-(PHANSO);
	PHANSO operator*(PHANSO);
	PHANSO operator/(PHANSO);
	bool operator==(PHANSO);
	void input();
	void output();
};
PHANSO::PHANSO(int ts, int ms){
	this->ts=ts;
	this->ms=ms;
}
PHANSO PHANSO::operator+(PHANSO x){
	PHANSO tmp;
	tmp.ts=ts* x.ms + x.ts* ms;
	tmp.ms=ms* x.ms;
	return tmp;
}
PHANSO PHANSO::operator-(PHANSO x){
	PHANSO tmp;
	tmp.ts=ts* x.ms - x.ts* ms;
	tmp.ms=ms* x.ms;
	return tmp;
}
PHANSO PHANSO::operator*(PHANSO x){
	PHANSO tmp;
	tmp.ts=ts* x.ts;
	tmp.ms=ms* x.ms;
	return tmp;
}
PHANSO PHANSO::operator/(PHANSO x){
	PHANSO tmp;
	tmp.ts=ts* x.ms;
	tmp.ms=ms* x.ts;
	return tmp;
}
void PHANSO::input(){
	cout << "Nhap tu so: ";
	cin >> ts;
	cout << "Nhap mau so: ";
	cin >> ms;
}
void PHANSO::output(){
	cout << ts << "/" << ms << endl;
}
bool PHANSO::operator==(PHANSO x){
	return (this->ts* x.ms == this->ms* x.ts);
}
int main(){
	PHANSO A,B,C;
	A.input();
	B.input();
	A.output();
	B.output();

	cout << "Tong: ";
	C=A+B;
	C.output();
	
	cout << "Hieu: ";
	C=A-B;
	C.output();
	
	cout << "Tich: ";
	C=A*B;
	C.output();
	
	cout << "Thuong: ";
	C=A/B;
	C.output();
	if(A==B){
		cout << "A bang B";
	}
	else{
		cout << "A khong bang B";
	}
}
