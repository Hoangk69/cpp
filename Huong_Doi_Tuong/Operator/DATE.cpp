#include<iostream>
using namespace std;

class DATE{
	private:
		int ngay, thang, nam;
	public:
		DATE();
		DATE(int,int,int);
		friend DATE operator+(DATE,int);
		bool operator==(DATE);
		friend istream & operator>>(istream &is, DATE &x);
		friend ostream & operator<<(ostream &os, DATE &x);	
};
DATE::DATE(){
	this->ngay=0;
	this->thang=0;
	this->nam=0;
}
DATE::DATE(int ngay, int thang, int nam){
	this->ngay=ngay;
	this->thang=thang;
	this->nam=nam;
}
DATE operator+(DATE x, int z){
	DATE tmp;
	tmp.ngay= x.ngay + z;
	tmp.thang= x.thang;
	tmp.nam= x.nam;
	return tmp;
}
istream & operator>>(istream &is, DATE &x){
	do{
		cout << "nhap ngay: ";
		is >> x.ngay;
	}
	while(x.ngay<1 || x.ngay >31);
	do{
		cout << "nhap thang: ";
		is >> x.thang;
	}
	while(x.thang<1 || x.thang >12);
	do{
		cout << "nhap nam: ";
		is >> x.nam;	
	}
	while(x.nam<1);
	return is;
}
ostream & operator<<(ostream &os, DATE &x){
	os << x.ngay << "/" << x.thang << "/" << x.nam << "\n"<< endl;
	return os;
}
bool DATE::operator==(DATE d){
	return (d.ngay==this->ngay && d.thang==this->thang && d.nam==this->nam);
}
int main(){
	DATE d1,d2,d3;
	cin >> d1;
	cout << d1;
	cin >> d2;
	cout << d2;
	d3=d1+10;
	cout << "cong 1 so nguyen vao ngay: " << d3;
	if (d1==d2){
		cout << "d1 bang d2" << endl;
	}
	else{
		cout << "d1 khong bang d2" << endl;
	}
	return 0;
}
