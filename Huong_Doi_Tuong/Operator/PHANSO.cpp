#include<iostream>
using namespace std;

class PHANSO{
private:
	int a, b;  // a:tu so, b:mau so
public:
	PHANSO(){
		a=0;
		b=1;
	}
	PHANSO(int a, int b){
		this->a=a;
		this->b=b;
	}
	PHANSO operator+(PHANSO); // ham tinh tong hai 2 phan so
	PHANSO operator-(PHANSO); // ham tinh hieu 2 phan so
	PHANSO operator/(PHANSO); // ham tinh thuong 2 phan so
	PHANSO operator*(PHANSO); // tinh tich 2 phan so
	
	void input();
	void output();
	void ketqua();
};
void PHANSO::input(){
	cout << "Nhap tu so: ";
	cin >> this->a;
	cout << "Nhap mau so: ";
	cin >> this->b;
}
PHANSO PHANSO::operator+(PHANSO x){
	PHANSO tmp;
	tmp.a = a* x.b + b* x.a;
	tmp.b = b* x.b;
	return tmp;
}
PHANSO PHANSO::operator-(PHANSO x){
	PHANSO tmp;
	tmp.a = a* x.b - b* x.a;
	tmp.b = b* x.b;
	return tmp;
}
PHANSO PHANSO::operator/(PHANSO x){
	PHANSO tmp;
	tmp.a= a* x.b;
	tmp.b= b* x.a;
	return tmp; 
}
PHANSO PHANSO::operator*(PHANSO x){
	PHANSO tmp;
	tmp.a= a* x.a;
	tmp.b= b* x.b;
	return tmp;
}
void PHANSO::output(){
	cout << "Phan so vua nhap: " << this->a << "/" << this->b << endl;
}
void PHANSO::ketqua(){
	cout << a << "/" << b << endl;
}
int main(){
	PHANSO A,B,C;
	A.input();
	A.output();
	B.input();
	B.output();
	C=A+B;  // C=A.operator(B);
	cout << "Tong 2 phan so: ";
	C.ketqua();
	C=A-B; 
	cout << "Hieu 2 phan so: ";
	C.ketqua();
	C=A*B;
	cout << "Tich 2 phan so: ";
	C.ketqua();
	C=A/B;
	cout << "Thuong 2 phan so: ";
	C.ketqua();
}
