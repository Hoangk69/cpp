#include<iostream>
#include<string>
#define MAX 100
using namespace std;

class Person{
	protected:
		string name;
		int age;
	public:
		Person(){
			this->name= "NULL";
			this->age=0;
		}
		Person(string, int);
		void Nhap();
		void Xuat();
};
Person::Person(string name, int age){
	this->name=name;
	this->age=age;
}
void Person::Nhap(){
	cout << "Nhap ten: ";
	getline(cin, name);
	cout << "Nhap tuoi: ";
	cin >> age;
}
void Person::Xuat(){
	cout << name <<" - "<< age ;
}
class Student : public Person{
	protected:
		string schoolname, maijor;
	public:	
		Student(){
			name= "NULL";
			age=0;
			schoolname = "NULL";
			maijor="NULL";
		}
		Student(string, int, string, string);
		void input();
		void output();
		string getschoolname(){
			return schoolname;
		}
};
Student::Student(string name,int age, string schoolname, string maijor){
	this->name=name;
	this->age=age;
	this->schoolname=schoolname;
	this->maijor=maijor;
	
}
void Student::input(){
	Person::Nhap();
	cout << "Nhap Ten truong: ";
	cin.ignore();
	getline(cin, schoolname);
	
	cout << "Nhap Chuyen nghanh: ";
	getline(cin, maijor);
	
}
void Student::output(){
	cout << " \n====================\n";
	Person::Xuat();	
	cout << " - " << schoolname << " - " << maijor<< endl;
}
int main(){
	int n;
	cout << "Nhap so sinh vien: ";
	cin>>n;
	cin.ignore();	
	Student A[MAX];
	for(int i=0; i<n; i++){
		A[i].input();
		cout << endl;
	}
//	for(int i=0; i<n; i++){
//		A[i].output();
//		cout << endl;
//	}
	for(int i=0; i<n; i++){
		if(A[i].getschoolname()=="HNUE"){
			A[i].output();
			cout << endl;
		}	
	}	
//	A.input();
//	A.output();
}
