#include<iostream>
#include<string>
#define MAX 100
using namespace std;

class NGUOI{
	protected:
		string ten;
		int tuoi;
		string gioitinh;
	public:
		NGUOI(){
			this->ten="NULL";
			this->tuoi=0;
			this->gioitinh="NULL";
		}
		NGUOI(string, int, string);
		void Nhap();
		void Xuat();
};
NGUOI::NGUOI(string ten, int tuoi, string gioitinh){
	this->ten=ten;
	this->tuoi=tuoi;
	this->gioitinh=gioitinh;
}
void NGUOI::Nhap(){
	cout << "Nhap ten: ";
	getline(cin, this->ten);
	cout << "Nhap tuoi: ";
	cin >> this->tuoi;
	cin.ignore();
	cout << "Nhap gioi tinh: ";
	getline(cin, this->gioitinh);
}
void NGUOI::Xuat(){
	cout << this->ten << " - " << this->tuoi << " - " << this->gioitinh;
}
class SINHVIEN : public NGUOI{
	protected:
		string msv;
		string tentruong;
	public:
		SINHVIEN(){
			this->ten="NULL";
			this->tuoi=0;
			this->gioitinh="NULL";
			this->msv="NULL";
			this->tentruong="NULL";	
		}
		SINHVIEN(string ten, int tuoi, string gioitinh, string msv, string tentruong){
			this->ten=ten;
			this->tuoi=tuoi;
			this->gioitinh=gioitinh;
			this->msv=msv;
			this->tentruong=tentruong;
		}
		void Nhap();
		void Xuat();	
};
void SINHVIEN::Nhap(){
	NGUOI::Nhap();
	cout << "Nhap msv: ";
	getline(cin, this->msv);
	cout << "Nhap ten truong: ";
	getline(cin, this->tentruong);
}
void SINHVIEN::Xuat(){
	NGUOI::Xuat();
	cout << " - " << msv << " - " << tentruong << endl;
	cout << "\n===============\n";
}
int main(){
	int n;
	cout << "Nhap so luong SV: ";
	cin >> n; 
	cin.ignore();
	SINHVIEN A[MAX];
	for(int i=0; i<n; i++){
		A[i].Nhap();
		cout << "\n==============\n";
	}
	for(int i=0; i<n; i++){
		A[i].Xuat();
	}
}
