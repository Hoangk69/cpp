#include<iostream>

using namespace std;

// khai bao cau truc 1 cai node
struct Node{
	int data;
	Node *next;
};

// khai bao cau truc danh sach lien ket
struct LinkedList{
	Node *Head;
	Node *Tail;
	int length;
};

// khoi tao 1 node
Node *CreatNode(int value){
	Node *temp = new Node;
	temp->data = value;
	temp->next = NULL;
	return temp;
}

// khoi tao list 
void CreatList(LinkedList& L){
	L.Head=NULL;
	L.Tail=NULL;
	L.length = 0;
}

void Insert(LinkedList& L, int value, int i){
	Node* P = CreatNode(value);
	if ( i== 1) // xen vao dau danh sach
	{
		P->next = L.Head;
		L.Head = P; 
	}
	else{
		Node *Q = L.Head;
		for(int k = 1; k < i-1; k++)
			Q = Q->next;
		P->next = Q->next;
		Q->next = P;
	}
	L.length++;	
}
// them vao dau danh sach
void Append(LinkedList &L, int value){
	Node* P = CreatNode(value);
	if(L.Head==NULL){
		L.Head = L.Tail = P;
	}
	else{
		P->next = L.Head;
		L.Head = P;
	}
	L.length++;
}
void Delete(LinkedList& L, int i){
	if(i<1 || i>L.length){
		return;	
	}
	Node *Q;
	if(i==1){
		Q=L.Head;
		L.Head = Q->next;
		delete Q;
	}
	else{
		Q=L.Head;
		for(int k=1; k<i-1; k++){
			Q=Q->next;
		}
		Node *x = Q->next;
		Q->next=Q->next->next;
		delete x;
	}
	L.length--;
}
void PrintList(LinkedList L){
	for(Node* k = L.Head; k!=NULL; k=k->next){
		cout << k->data << " ";
	}
//	Node* Curr = L.Head;
//	
//	while(Curr != NULL){
//		cout << "   " << Curr->data;
//		Curr = Curr->next;
//	}
}

int main(){
	LinkedList L;
	
	CreatList(L);
	
	Insert(L,5,1);
	Insert(L,7,1);
	Insert(L,9,1);
	Insert(L,2,2);
	Append(L,10);

	PrintList(L);
	
	
	cout << "\nSo phan tu danh sach: " << L.length << endl;
	Delete(L,3);
	
	PrintList(L);
	
	cout << "\nSo phan tu danh sach: " << L.length << endl;
	return 0;	
}
